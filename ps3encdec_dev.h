
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _PS3ENCDEC_DEV_H_
#define _PS3ENCDEC_DEV_H_

#include <stdint.h>

int ps3encdec_dev_open(const char *path);

int ps3encdec_dev_close(int fd);

int ps3encdec_dev_do_request(int fd, uint64_t cmd, uint8_t *cmdbuf,
	uint64_t cmdbuf_size, uint8_t *respbuf, uint64_t respbuf_size);

#endif
