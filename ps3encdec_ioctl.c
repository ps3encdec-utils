
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#include "ps3encdec_dev.h"

#define PS3ENCDEC_IOCTL_VERSION		"0.0.1"

struct opts
{
	char *device_name;
	char *cmd;
	int do_help;
	int do_verbose;
	int do_version;
};

static struct option long_opts[] = {
	{ "help",	no_argument, NULL, 'h' },
	{ "verbose",	no_argument, NULL, 'v' },
	{ "version",	no_argument, NULL, 'V' },
	{ NULL, 0, NULL, 0 }
};

/*
 * usage
 */
static void usage(void) {
	fprintf(stderr,
		"Usage: ps3encdec_ioctl [OPTIONS] DEVICE [ARGS]\n"
		"\n"
		"Options:\n"
		"	-h, --help			Show this message and exit\n"
		"	-v, --verbose			Increase verbosity\n"
		"	-V, --version			Show version information and exit\n"
		"Commands:\n"
		"	kgen1 ARG1			EdecKgen1\n"
		"	kgenflash			EdecKgenFlash\n"
		"\n\n"
		"Simple example: EdecKgen1:\n"
		"	ps3encdec_ioctl /dev/ps3encdec kgen1 0x00 0x01 0x00 0x30 0x72 0xA7 0x88 0xEC \\\n"
		"		0xFC 0xA4 0x06 0x71 0x4C 0xB1 0x50 0xC9 0xFB 0xE0 0x06 0xC2 0x74 0xB5 \\\n"
		"		0x84 0xC4 0xE6 0xBD 0x1E 0x55 0x4E 0x36 0xE9 0xC9 0xD6 0x09 0xBC 0xB4 \\\n"
		"		0x79 0xA6 0xBC 0xDE 0x60 0xA5 0xB2 0x41 0xC7 0x15 0x68 0x68 0x82 0x1D \\\n"
		"		0x8F 0xD6 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00\n");
}

/*
 * version
 */
static void version(void)
{
	fprintf(stderr,
		"ps3encdec_ioctl " PS3ENCDEC_IOCTL_VERSION "\n"
		"Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>\n"
		"This is free software.  You may redistribute copies of it "
		"under the terms of\n"
		"the GNU General Public License 2 "
		"<http://www.gnu.org/licenses/gpl2.html>.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n");
}

/*
 * process_opts
 */
static int process_opts(int argc, char **argv, struct opts *opts)
{
	int c;

	while ((c = getopt_long(argc, argv, "hvV", long_opts, NULL)) != -1) {
		switch (c) {
		case 'h':
		case '?':
			opts->do_help = 1;
			return 0;

		case 'v':
			opts->do_verbose++;
			break;

		case 'V':
			opts->do_version = 1;
			return 0;

		default:
			fprintf(stderr, "Invalid command option: %c\n", c);
			return -1;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "No device specified\n");
		return -1;
	}

	opts->device_name = argv[optind];
	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No command specified\n");
		return -1;
	}

	opts->cmd = argv[optind];
	optind++;

	return 0;
}

/*
 * cmd_kgen1
 */
static int cmd_kgen1(int fd, struct opts *opts, int argc, char **argv)
{
	uint32_t val;
	uint64_t cmdbuf_size;
	uint8_t cmdbuf[0x40];
	uint8_t respbuf[0x50];
	char *endptr;
	int error, i;

	if (optind >= argc) {
		fprintf(stderr, "No ARG1 specified\n");
		return -1;
	}

	cmdbuf_size = 0;

	while (optind < argc) {
		val = strtoul(argv[optind], &endptr, 0);
		if ((*endptr != '\0') || (val > 0xff)) {
			fprintf(stderr, "Invalid ARG1 specified: %s\n", argv[optind]);
			return -1;
		}

		optind++;

		cmdbuf[cmdbuf_size++] = val;

		if (cmdbuf_size == 0x40)
			break;
	}

	if (cmdbuf_size < 0x40) {
		fprintf(stderr, "ARG1 should be of size 0x40 bytes\n");
		return -1;
	}

	error = ps3encdec_dev_do_request(fd, 0x81, cmdbuf, sizeof(cmdbuf), respbuf, sizeof(respbuf));

	if (error) {
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	} else {
		for (i = 0; i < sizeof(respbuf); i++)
			fprintf(stdout, "0x%02x ", respbuf[i]);

		fprintf(stdout, "\n");
	}

	return error;
}

/*
 * cmd_kgenflash
 */
static int cmd_kgenflash(int fd, struct opts *opts, int argc, char **argv)
{
	int error;

	error = ps3encdec_dev_do_request(fd, 0x84, NULL, 0, NULL, 0);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));

	return error;
}

/*
 * main
 */
int main(int argc, char **argv)
{
	struct opts opts;
	int fd = 0, error = 0;

	memset(&opts, 0, sizeof(opts));

	if (process_opts(argc, argv, &opts)) {
		usage();
		error = 1;
		goto done;
	}

	if (opts.do_help) {
		usage();
		goto done;
	} else if (opts.do_version) {
		version();
		goto done;
	}

	fd = ps3encdec_dev_open(opts.device_name);
	if (fd < 0) {
		fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
		error = 2;
		goto done;
	}

	if (!strcmp(opts.cmd, "kgen1")) {
		error = cmd_kgen1(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "kgenflash")) {
		error = cmd_kgenflash(fd, &opts, argc, argv);
	} else {
		usage();
		error = 1;
		goto done;
	}

	if (error)
		error = 3;

done:

	if (fd >= 0)
		ps3encdec_dev_close(fd);

	exit(error);
}
