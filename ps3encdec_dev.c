
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <asm/ps3encdec.h>

#include "ps3encdec_dev.h"

int ps3encdec_dev_open(const char *path)
{
	return open(path, O_RDWR);
}

int ps3encdec_dev_close(int fd)
{
	return close(fd);
}

int ps3encdec_dev_do_request(int fd, uint64_t cmd, uint8_t *cmdbuf,
	uint64_t cmdbuf_size, uint8_t *respbuf, uint64_t respbuf_size)
{
	struct ps3encdec_ioctl_do_request arg;
	int error;

	memset(&arg, 0, sizeof(arg));
	arg.cmd = cmd;
	arg.cmdbuf = (uint64_t) cmdbuf;
	arg.cmdbuf_size = cmdbuf_size;
	arg.respbuf = (uint64_t) respbuf;
	arg.respbuf_size = respbuf_size;

	error = ioctl(fd, PS3ENCDEC_IOCTL_DO_REQUEST, &arg);
	
	return error;
}
