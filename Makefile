
CC=gcc
CFLAGS=-Wall -O2 -g -m64
LDFLAGS=-m64

SRC_ps3encdec_ioctl=ps3encdec_dev.c ps3encdec_ioctl.c
OBJ_ps3encdec_ioctl=$(SRC_ps3encdec_ioctl:.c=.o)
TARGET_ps3encdec_ioctl=ps3encdec_ioctl

all: $(TARGET_ps3encdec_ioctl)

$(TARGET_ps3encdec_ioctl): $(OBJ_ps3encdec_ioctl)
	$(CC) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(OBJ_ps3encdec_ioctl) $(TARGET_ps3encdec_ioctl)
